from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse


def index(request):
    return render(request,"profile_page/index.html")

def news(request):
    return render(request,"profile_page/news.html")