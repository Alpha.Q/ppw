SHELL := /bin/bash

default: clean setup deployment runserver

clean:
	find . -name '*.pyc' -delete;\

setup: requirements.txt
	python3 -m venv env;\
	source env/bin/activate;\
	pip install -r requirements.txt;\

deployment: collectstatic database

collectstatic:
	source env/bin/activate;\
	python manage.py collectstatic;\

database:
	source env/bin/activate;\
	python manage.py makemigrations profile_page;\
	python manage.py migrate;\

runserver:
	source env/bin/activate;\
	python manage.py runserver;\


