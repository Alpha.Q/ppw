function getNews(){
    var div = document.getElementById("news")

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", "https://newsapi.org/v2/top-headlines?country=id&apiKey=fab52125e09345d195306c90186217fa", true);
    xmlHttp.responseType = "json";
    xmlHttp.send();

    xmlHttp.onload = function(){
        if(xmlHttp.status==200){
            var respon = xmlHttp.response;
            var totalResult = respon.totalResults;
            
            var index = getRandInteger(0, Math.min(20, totalResult) - 1);
            console.log(index);

            var title = "";
            if(respon.articles[index].title != null){
                var title = "<h2>" + respon.articles[index].title + "</h2>";
            }
            var img = "<img src=\"" + respon.articles[index].urlToImage + "\" alt=\"failed\" class=\"news-image\">";
            var shortDesc = "<h3 class=\"text-color2\">" + respon.articles[index].description + "</h3>";
            var date = "<h4>Dipublikasikan pada : " + formatTime(new Date(respon.articles[index].publishedAt)) + "</h4>";
            
            var readMore = "<a href=\" " + respon.articles[index].url + " \"  width=\"50%\">" + "Baca Selengkapnya disini</a>"
            div.innerHTML = title+img+shortDesc+date+readMore;
        }else{
            div.innerHTML = "call failed, reason : " + xmlHttp.statusText;
        }
    }
}

function getRandInteger(min, max){
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}

function formatTime(time){
    var monthNames = [
        "Januari", "Februari", "Maret",
        "April", "Mei", "Juni", "Juli",
        "Agustus", "September", "Oktober",
        "November", "Desember"
    ];

    var day = time.getDate();
    var monthIndex = time.getMonth();
    var year = time.getFullYear();
    var hours = time.getHours();
    var minute = time.getMinutes();
    var second = time.getSeconds();

    if(hours < 10){
        hours = "0" + hours;
    }

    if(minute < 10){
        minute = "0" + minute;
    }

    if(second < 10){
        second = "0" + second;
    }

    return day + ' ' + monthNames[monthIndex] + ' ' + year + ' pukul: ' +  hours + ":" + minute + ":" + second;
}